package com.example.twelfthtask

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.twelfthtask.databinding.CountriesBinding
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

class RecyclerViewAdapter() : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    private val countries = mutableListOf<MyCountryModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
       return ItemViewHolder(CountriesBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return countries.size
    }

    inner class ItemViewHolder(private val binding : CountriesBinding) : RecyclerView.ViewHolder(binding.root){
        private lateinit var model: MyCountryModel
        fun bind(){
            model = countries[adapterPosition]
            loadSvg(model.flag)
        }

        private fun loadSvg(url: String?) {
            GlideToVectorYou
                .init()
                .with(binding.root.context)
                .load(Uri.parse(url), binding.flag)
        }
    }

    fun setter(countries : MutableList<MyCountryModel>){
        this.countries.clear()
        this.countries.addAll(countries)
        notifyDataSetChanged()
    }

    fun clearData(){
        this.countries.clear()
        notifyDataSetChanged()
    }
}