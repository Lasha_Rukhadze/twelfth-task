package com.example.twelfthtask

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twelfthtask.databinding.FragmentFirstBinding

class FirstFragment : BaseFragment<FragmentFirstBinding, CountriesViewModel>(FragmentFirstBinding::inflate, CountriesViewModel::class.java) {

    override var binding: FragmentFirstBinding? = null
    private val viewModel : CountriesViewModel by viewModels()
    private lateinit var adapter: RecyclerViewAdapter

    /*override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       // binding = FragmentFirstBinding.inflate(inflater, container, false)
        initializer()

        return binding!!.root
    }*/

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        initializer()
    }

    private fun initializer(){
        viewModel.init()
        recycler()
        observes()

        binding?.refresh?.setOnRefreshListener {
            adapter.clearData()
            viewModel.init()
        }
    }

    private fun recycler(){
        adapter = RecyclerViewAdapter()
        binding?.recycler?.layoutManager = LinearLayoutManager(requireActivity())
        binding?.recycler?.adapter = adapter
    }

    private fun observes(){
        viewModel.getLoadingLiveData().observe(viewLifecycleOwner, {
            binding?.refresh?.isRefreshing ?: it
        })
        viewModel.getCountriesLiveData().observe(viewLifecycleOwner,{
            adapter.setter(it.toMutableList())
        })
    }


}