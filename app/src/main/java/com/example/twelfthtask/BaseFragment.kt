package com.example.twelfthtask

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding

typealias Inflate <T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VBinding : ViewBinding, VM : ViewModel>(private val inflate: Inflate<VBinding>, viewModel: Class<CountriesViewModel>
) : Fragment() {

    open var binding : VBinding? = null
    var mainActivity : MainActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (binding == null){
            binding = inflate(inflater, container, false)
        }

        return binding!!.root
    }

    override fun onAttach(context: Context) {
        if (context is MainActivity) { mainActivity = context }
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        mainActivity = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    abstract fun start(inflater: LayoutInflater, container: ViewGroup?)
}