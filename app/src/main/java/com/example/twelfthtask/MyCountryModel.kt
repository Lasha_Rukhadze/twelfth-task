package com.example.twelfthtask

data class MyCountryModel(var name : String,
                          var topLevelDomain : String,
                          var capital : String,
                          var flag : String) {

}
