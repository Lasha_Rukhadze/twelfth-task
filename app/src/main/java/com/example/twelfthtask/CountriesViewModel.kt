package com.example.twelfthtask

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CountriesViewModel : ViewModel() {

    private val countriesLiveData = MutableLiveData<List<MyCountryModel>>().apply {
        mutableListOf<MyCountryModel>()
    }

   // val _countriesLiveData : LiveData<List<MyCountryModel>> = countriesLiveData
   fun getCountriesLiveData() : LiveData<List<MyCountryModel>> {
       return countriesLiveData
   }

    private val loadingLiveData = MutableLiveData<Boolean>().apply {
        true
    }

    //val _loadingLiveData : LiveData<Boolean> = loadingLiveData
    fun getLoadingLiveData() : LiveData<Boolean> {
        return loadingLiveData
    }

    fun init(){
        /*CoroutineScope(Dispatchers.IO).launch {
            getCountries()
        }*/
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getCountries()
            }
        }
    }

    private suspend fun getCountries() {
        val response = RetrofitService.retrofitService().getCountry()

        if (response.isSuccessful) {
            val countries = response.body()
            countriesLiveData.postValue(countries)
        } else {
            response.code()
        }
        loadingLiveData.postValue(false)
    }

}